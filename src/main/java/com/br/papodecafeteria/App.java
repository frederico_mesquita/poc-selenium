package com.br.papodecafeteria;

import java.time.Duration;
import java.time.Instant;
import java.util.logging.Level;

import org.openqa.selenium.chrome.ChromeDriver;

import com.br.papodecafeteria.integration.Commom;
import com.br.papodecafeteria.integration.UseCaseFactory;

public class App extends Commom {
	
	public static void main( String[] args ) {
        
        Instant inicio = Instant.now();
        ChromeDriver webDriver = null;
        
        try {
        	System.setProperty("webdriver.chrome.driver", getProperties("resources/application.property", "webdriver.chrome.driver"));

        	webDriver = new ChromeDriver();
        	webDriver.manage().window().maximize();

        	UseCaseFactory ucFactory = UseCaseFactory.getInstance();        	
        	for(Object usecase : getProperties("usecase/usecaselist.property").values())
        		ucFactory.getUseCase(usecase.toString()).execute(webDriver);
        	ucFactory = null;
        	
        	webDriver.close();
        	webDriver = null;
        } catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		} finally {
			System.clearProperty("webdriver.chrome.driver");
		}
		System.out.println("Tempo total de execução da bateria de testes: " + Duration.between(inicio, Instant.now()).toMillis() + " ms");
    }
}
