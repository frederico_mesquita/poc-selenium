package com.br.papodecafeteria.integration;

import java.util.logging.Level;

import com.br.papodecafeteria.usecase.AddUser;
import com.br.papodecafeteria.usecase.DeleteUser;
import com.br.papodecafeteria.usecase.EditUser;

public class UseCaseFactory extends Commom {
	
	private static UseCaseFactory instance = null;
	
	private UseCaseFactory(){}
	
	public static UseCaseFactory getInstance(){
		if (null == instance)
			instance = new UseCaseFactory();
		return instance;
	}
	
	public IUseCase getUseCase(String pUseCase){
		try{
			if(pUseCase.equalsIgnoreCase("adduser"))
				return AddUser.getInstance();
			else if(pUseCase.equalsIgnoreCase("edituser"))
				return EditUser.getInstance();
			else if(pUseCase.equalsIgnoreCase("deleteuser"))
				return DeleteUser.getInstance();
		}catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		return null;
	}

}
