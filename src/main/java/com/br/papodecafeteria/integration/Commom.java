package com.br.papodecafeteria.integration;

import java.io.FileInputStream;
import java.time.Instant;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Commom {
	private static Logger logger = Logger.getLogger(Commom.class.getName());
	protected String cValueExpected = "";
	protected String cValueReceived = "";
	protected Instant t0 = null;
	protected Instant t1 = null;
	protected static Properties properties = null;

	public static Logger getLogger() {
		return logger;
	}
	
	protected static void registerLog(Level pLevel, String pExpected, String pReceived){
		if(!pExpected.equalsIgnoreCase(pReceived))
			getLogger().log(pLevel, 
        			"Expected <" + pExpected + "> Received <" + pReceived + ">");
	}

	protected void clear(){
		setcValueExpected("");
		setcValueReceived("");
		setT0(null);
		setT1(null);
		properties = null;
	}
	
	protected static void loadProperties(String pReference){
		try {
			FileInputStream fileInputStream = new FileInputStream(pReference);
			properties = new Properties();
			properties.load(fileInputStream);
			fileInputStream = null;
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
	}
	
	protected static String getProperty(String pProperty){
		String cReturn = "";
		try {
			cReturn = properties.getProperty(pProperty);
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	protected static String getProperties(String pReference, String pProperty){
		String cReturn = "";
		try {
			Properties properties = new Properties();
			FileInputStream fileInputStream = new FileInputStream(pReference);
			properties.load(fileInputStream);
			cReturn = properties.getProperty(pProperty);
			fileInputStream = null;
			properties = null;
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		return cReturn;
	}
	
	protected static Map<String, String> getProperties(String pReference){
		Properties properties = null;
		Map<String, String> sortedMap = null;
		try {
			properties = new Properties();
			FileInputStream fileInputStream = new FileInputStream(pReference);
			properties.load(fileInputStream);	
			sortedMap = new TreeMap(properties);
			fileInputStream = null;
			properties = null;
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		return sortedMap;
	}

	public String getcValueExpected() {
		return cValueExpected;
	}

	public void setcValueExpected(String cValueExpected) {
		this.cValueExpected = cValueExpected;
	}

	public String getcValueReceived() {
		return cValueReceived;
	}

	public void setcValueReceived(String cValueReceived) {
		this.cValueReceived = cValueReceived;
	}

	public Instant getT0() {
		return t0;
	}

	public void setT0(Instant t0) {
		this.t0 = t0;
	}

	public Instant getT1() {
		return t1;
	}

	public void setT1(Instant t1) {
		this.t1 = t1;
	}
}
