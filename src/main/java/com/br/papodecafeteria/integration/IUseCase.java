package com.br.papodecafeteria.integration;

import org.openqa.selenium.WebDriver;

public interface IUseCase {
	void execute(WebDriver pWebDriver);	
	void validate(WebDriver pWebDriver);
}
