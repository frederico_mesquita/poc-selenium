package com.br.papodecafeteria.usecase;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.br.papodecafeteria.integration.Commom;
import com.br.papodecafeteria.integration.IUseCase;

public class DeleteUser extends Commom implements IUseCase {
	
	private static DeleteUser instance = null;
	
	private DeleteUser(){}
	
	public static DeleteUser getInstance(){
		if(null == instance)
			instance = new DeleteUser();
		return instance;
	}

	public void execute(WebDriver pWebDriver){
		clear();
		setT0(Instant.now());
		loadProperties("useCase/deleteuser.property");
		System.out.println(getProperty("deleteuser.value")+ " - Início...");
		try{
			boolean bVldt = false;
			pWebDriver.get(getProperties("resources/application.property", "application.url"));
			pWebDriver.findElement(By.id(getProperty("deleteuser.button.navigateTo"))).click();
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperty("deleteuser.html.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			
			setcValueExpected(getProperty("deleteuser.input.name.value"));
			List<WebElement> lstLines = pWebDriver.findElements(By.id(getProperties("resources/application.property", "application.table.line.id")));
			for(WebElement line : lstLines){
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_1.id"))).getText());
				if(getcValueReceived().equalsIgnoreCase(getcValueExpected())){
					bVldt = true;
					line.findElement(By.id(getProperty("deleteuser.button.id"))).click();
					break;
				}
			}
			
			if(!bVldt){
				String cVldt = "";
				cVldt += "<" + getProperty("deleteuser.input.name.value") + ">";
				cVldt += "<" + getProperty("deleteuser.input.email.value") + ">";
				cVldt += "<" + getProperty("deleteuser.input.sex.value") + ">";
				cVldt += "<" + getProperty("deleteuser.input.country.value") + ">";
				registerLog(Level.SEVERE, cVldt, "<>");
			}
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperty("deleteuser.html.edited.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			//validate(pWebDriver);
			
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		setT1(Instant.now());
		System.out.println(getProperty("deleteuser.value")+ " - Fim: Tempo total de execução de " + Duration.between(getT0(), getT1()).toMillis() + " ms");
	}
	
	public void validate(WebDriver pWebDriver){
		boolean bVldt = false;
		pWebDriver.get(getProperties("resources/application.property", "application.url"));
		pWebDriver.findElement(By.id(getProperty("deleteuser.button.navigateTo"))).click();
		
		setcValueExpected(pWebDriver.getTitle());
		setcValueReceived(getProperty("deleteuser.html.deleted.title"));
		if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
			registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
		
		setcValueExpected(getProperty("deleteuser.input.name.value"));
		List<WebElement> lstLines = pWebDriver.findElements(By.id(getProperties("resources/application.property", "application.table.line.id")));
		for(WebElement line : lstLines){
			setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_1.id"))).getText());
			if(getcValueReceived().equalsIgnoreCase(getcValueExpected())){
				bVldt = true;
				
				setcValueExpected(getProperty("deleteuser.input.email.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_2.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
				setcValueExpected(getProperty("deleteuser.input.sex.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_3.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
				setcValueExpected(getProperty("deleteuser.input.country.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_4.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
			}
		}
		
		if(!bVldt){
			String cVldt = "";
			cVldt += "<" + getProperty("deleteuser.input.name.value") + ">";
			cVldt += "<" + getProperty("deleteuser.input.email.value") + ">";
			cVldt += "<" + getProperty("deleteuser.input.sex.value") + ">";
			cVldt += "<" + getProperty("deleteuser.input.country.value") + ">";
			registerLog(Level.SEVERE, cVldt, "<>");
		}
	}
}