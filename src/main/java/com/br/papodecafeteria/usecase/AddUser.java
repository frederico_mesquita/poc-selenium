package com.br.papodecafeteria.usecase;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.br.papodecafeteria.integration.Commom;
import com.br.papodecafeteria.integration.IUseCase;

public class AddUser extends Commom implements IUseCase {
	
	private static AddUser instance = null;
	
	private AddUser(){}
	
	public static AddUser getInstance(){
		if(null == instance)
			instance = new AddUser();
		return instance;
	}

	public void execute(WebDriver pWebDriver){
		clear();
		setT0(Instant.now());
		loadProperties("useCase/adduser.property");
		System.out.println(getProperty("adduser.value") + " - Início...");
		try{
			pWebDriver.get(getProperties("resources/application.property", "application.url"));
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperties("resources/application.property", "application.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			
			pWebDriver.findElement(By.id(getProperty("adduser.button.id"))).click();
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperty("adduser.html.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());

			pWebDriver.findElement(By.id(getProperty("adduser.input.name.id"))).sendKeys(getProperty("adduser.input.name.value"));
			pWebDriver.findElement(By.id(getProperty("adduser.input.password.id"))).sendKeys(getProperty("adduser.input.password.value"));
			pWebDriver.findElement(By.id(getProperty("adduser.input.email.id"))).sendKeys(getProperty("adduser.input.email.value"));
			pWebDriver.findElement(By.id(getProperty("adduser.input.sex.id"))).click();
			
			Select dropdown = new Select(pWebDriver.findElement(By.id(getProperty("adduser.input.country.id"))));
			dropdown.selectByVisibleText(getProperty("adduser.input.country.value"));
			
			pWebDriver.findElement(By.id(getProperty("adduser.form.submit.id"))).submit();
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperty("adduser.html.added.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			
			setcValueExpected(pWebDriver.findElement(By.id(getProperty("adduser.html.msg-success.id"))).getText());
			setcValueReceived(getProperty("adduser.html.msg-success.value"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			
			validate(pWebDriver);
			
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		setT1(Instant.now());
		System.out.println(getProperty("adduser.value")+ " - Fim: Tempo total de execução de " + Duration.between(getT0(), getT1()).toMillis() + " ms");
	}
	
	public void validate(WebDriver pWebDriver){
		boolean bVldt = false;
		
		pWebDriver.findElement(By.id(getProperty("adduser.button.navigateTo"))).click();
		
		setcValueExpected(pWebDriver.getTitle());
		setcValueReceived(getProperty("adduser.html.viewusers.title"));
		if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
			registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
		
		setcValueExpected(getProperty("adduser.input.name.value"));
		List<WebElement> lstLines = pWebDriver.findElements(By.id(getProperties("resources/application.property", "application.table.line.id")));
		for(WebElement line : lstLines){
			setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_1.id"))).getText());
			if(getcValueReceived().equalsIgnoreCase(getcValueExpected())){
				bVldt = true;
				
				setcValueExpected(getProperty("adduser.input.email.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_2.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
				setcValueExpected(getProperty("adduser.input.sex.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_3.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
				setcValueExpected(getProperty("adduser.input.country.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_4.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
			}
		}

		if(!bVldt){
			String cVldt = "";
			cVldt += "<" + getProperty("adduser.input.name.value") + ">";
			cVldt += "<" + getProperty("adduser.input.email.value") + ">";
			cVldt += "<" + getProperty("adduser.input.sex.value") + ">";
			cVldt += "<" + getProperty("adduser.input.country.value") + ">";
			registerLog(Level.SEVERE, cVldt, "<>");
		}
	}
}
