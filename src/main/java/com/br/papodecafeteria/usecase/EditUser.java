package com.br.papodecafeteria.usecase;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.br.papodecafeteria.integration.Commom;
import com.br.papodecafeteria.integration.IUseCase;

public class EditUser extends Commom implements IUseCase {
	
	private static EditUser instance = null;
	
	private EditUser(){}
	
	public static EditUser getInstance(){
		if(null == instance)
			instance = new EditUser();
		return instance;
	}

	public void execute(WebDriver pWebDriver){
		clear();
		setT0(Instant.now());
		loadProperties("useCase/edituser.property");
		System.out.println(getProperty("edituser.value")+ " - Início...");
		try{
			boolean bVldt = false;
			pWebDriver.get(getProperties("resources/application.property", "application.url"));
			pWebDriver.findElement(By.id(getProperty("edituser.button.navigateTo"))).click();
			
			setcValueExpected(getProperty("edituser.input.name.value"));
			List<WebElement> lstLines = pWebDriver.findElements(By.id(getProperties("resources/application.property", "application.table.line.id")));
			for(WebElement line : lstLines){
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_1.id"))).getText());
				if(getcValueReceived().equalsIgnoreCase(getcValueExpected())){
					bVldt = true;
					line.findElement(By.id(getProperty("edituser.button.id"))).click();
					break;
				}
			}
			
			if(!bVldt){
				String cVldt = "";
				cVldt += "<" + getProperty("edituser.input.name.value") + ">";
				cVldt += "<" + getProperty("edituser.input.email.value") + ">";
				cVldt += "<" + getProperty("edituser.input.sex.value") + ">";
				cVldt += "<" + getProperty("edituser.input.country.value") + ">";
				registerLog(Level.SEVERE, cVldt, "<>");
			}
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperty("edituser.html.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			
			pWebDriver.findElement(By.id(getProperty("edituser.input.name.id"))).clear();
			pWebDriver.findElement(By.id(getProperty("edituser.input.name.id"))).sendKeys(getProperty("edituser.input.newname.value"));
			
			pWebDriver.findElement(By.id(getProperty("edituser.input.email.id"))).clear();
			pWebDriver.findElement(By.id(getProperty("edituser.input.email.id"))).sendKeys(getProperty("edituser.input.newemail.value"));
			
			Select dropdown = new Select(pWebDriver.findElement(By.id(getProperty("edituser.input.country.id"))));
			dropdown.selectByVisibleText(getProperty("edituser.input.newcountry.value"));
			
			pWebDriver.findElement(By.id(getProperty("edituser.form.submit.id"))).submit();
			
			setcValueExpected(pWebDriver.getTitle());
			setcValueReceived(getProperty("edituser.html.edited.title"));
			if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
				registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
			
			validate(pWebDriver);
			
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, exc.getMessage(), exc);
		}
		setT1(Instant.now());
		System.out.println(getProperty("edituser.value")+ " - Fim: Tempo total de execução de " + Duration.between(getT0(), getT1()).toMillis() + " ms");
	}
	
	public void validate(WebDriver pWebDriver){
		boolean bVldt = false;
		pWebDriver.get(getProperties("resources/application.property", "application.url"));
		pWebDriver.findElement(By.id(getProperty("edituser.button.navigateTo"))).click();
		
		setcValueExpected(pWebDriver.getTitle());
		setcValueReceived(getProperty("edituser.html.edited.title"));
		if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
			registerLog(Level.WARNING, getcValueExpected(), getcValueReceived());
		
		setcValueExpected(getProperty("edituser.input.newname.value"));
		List<WebElement> lstLines = pWebDriver.findElements(By.id(getProperties("resources/application.property", "application.table.line.id")));
		for(WebElement line : lstLines){
			setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_1.id"))).getText());
			if(getcValueReceived().equalsIgnoreCase(getcValueExpected())){
				bVldt = true;
				
				setcValueExpected(getProperty("edituser.input.newemail.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_2.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
				setcValueExpected(getProperty("edituser.input.sex.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_3.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
				setcValueExpected(getProperty("edituser.input.newcountry.value"));
				setcValueReceived(line.findElement(By.id(getProperties("resources/application.property", "application.table.line.data_4.id"))).getText());
				if(!getcValueExpected().equalsIgnoreCase(getcValueReceived()))
					registerLog(Level.SEVERE, getcValueExpected(), getcValueReceived());
				
			}
		}

		if(!bVldt){
			String cVldt = "";
			cVldt += "<" + getProperty("edituser.input.newname.value") + ">";
			cVldt += "<" + getProperty("edituser.input.newemail.value") + ">";
			cVldt += "<" + getProperty("edituser.input.sex.value") + ">";
			cVldt += "<" + getProperty("edituser.input.newcountry.value") + ">";
			registerLog(Level.SEVERE, cVldt, "<>");
		}
	}
}
