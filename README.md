PoC Selenium

Proof of concept of Selenium Webdriver tool to automated an battery of tests of an application.

Getting Started

Make a copy of source code and import to your preferred IDE. This is a maven project. After imported you just need to execute mvn clean install.
You may update the OS path to webdriver executable. Don´t forget update the property file as well.

Prerequisites
Java version "1.8.0_45"
Java(TM) SE Runtime Environment (build 1.8.0_45-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.45-b02, mixed mode)
Maven-3.5.0

To use this PoC you is necessary have an up and running web application. I have used a simple web CRUD composed by the following use cases:
* Add user
* Edit user
* Delete user

The properties files will contain the necessary itens for each use case as well will contain generic parameters for this application.

To use this test application you have to add you own code for each use case you have. I provided the basic examples and configurations to help you in this matter.

After execute the test application your console must show the lines below:
Starting ChromeDriver 2.32.498550 (9dec58e66c31bcc53a9ce3c7226f0c1c5810906a) on port 14422
Only local connections are allowed.
set 17, 2017 2:28:56 AM org.openqa.selenium.remote.ProtocolHandshake createSession
INFORMAÇÕES: Detected dialect: OSS
ADD USER - Início...
ADD USER - Fim: Tempo total de execução de 6843 ms
EDIT USER - Início...
EDIT USER - Fim: Tempo total de execução de 2755 ms
DELETE USER - Início...
DELETE USER - Fim: Tempo total de execução de 1105 ms
Tempo total de execução da bateria de testes: 16590 ms

Built With
Java version "1.8.0_45"
Java(TM) SE Runtime Environment (build 1.8.0_45-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.45-b02, mixed mode)
Maven-3.5.0
Eclipse Neon IDE

Versioning

1.0-SNAPSHOT

Authors

Frederico Mesquita (frederico_mesquita@hotmail.com)

License

This project is licensed under the MIT License - see the LICENSE.md file for details
